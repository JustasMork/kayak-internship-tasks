import React, { Component } from 'react';
import IconCheck from '../styles/icons/check.svg';

class FilterDropdownItem extends Component{

    constructor(props){
        super(props);
        this.state = this.props.itemData;
    }

    componentDidUpdate(){
        if(this.state.selected != this.props.itemData.selected)
            this.setState(this.props.itemData);
    }

    toggleCheckbox = () => {
        this.state.selected = !this.state.selected;
        this.props.selectHandler(this.props.id, this.state.selected);
        this.setState(this.state);
    }

    render(){
        return(
            <div className="dropdown-item">
                <div className="checkbox" onClick={this.toggleCheckbox}>
                    {this.state.selected ? (<img className="checkIcon" src={IconCheck}/>) : null}
                </div>
                <span className="dropdown-title">{this.state.title}</span>
                <span className="dropdown-subtitle">{this.state.subtitle}</span>
            </div>
        );
    }
}

export default FilterDropdownItem;