import React, { Component } from 'react';
import FilterButton from './FilterButton';
import FilterDropdown from './FilterDropdown';

class FilterMenu extends Component{

    constructor(props){
        super(props);
        this.state = this.props.btnData;
    }

    getButtons = () => {
        this.state = Object.values(this.state);
        var buttons = this.state.map((item, index) => {
            if(item.hasOwnProperty('dropdown'))
                return <FilterDropdown btnData={item} key={index} id={index} selectHandler={this.buttonClickHandler}/>
            else
                return <FilterButton btnData={item} key={index} id={index} clickHandler={this.buttonClickHandler}/>
        });
        return <div className="d-inline">{buttons}</div>
    };

    buttonClickHandler = (index) => {
        if(this.state[index].active){
            this.state[index].active = false;
        } else {
            this.state[index].active = true;
        }
        this.disableOtherButtons(index);
        this.setState(this.state);
    }

    disableOtherButtons = (index) => {
        if(this.state[index].hasOwnProperty('allButton'))
            for(var itemIndex in this.state){
                if(parseInt(itemIndex) != index)
                    this.state[itemIndex].active = false;
                if(this.state[itemIndex].hasOwnProperty('dropdown'))
                    this.state[itemIndex] = this.deselectAllDropdownItems(this.state[itemIndex]);
            }
        else
            for(var itemIndex in this.state){
                if(this.state[itemIndex].hasOwnProperty('allButton')){
                    this.state[itemIndex].active = false;
                    break;
                }
            }
    }

    deselectAllDropdownItems = (item) => {
        for(var itemIndex in item.dropdown){
            item.dropdown[itemIndex].selected = false;
        }
        return item;
    }

    render(){
        return(
            <div className="filter-menu-wrapper">
                {this.getButtons()}
            </div>
        )
    }

}

export default FilterMenu;