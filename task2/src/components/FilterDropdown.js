import React, { Component } from 'react';
import FilterDropdownItem from './FilterDropdownItem';
import IconArrowUp from '../styles/icons/arrow_up.svg';
import IconArrowDown from '../styles/icons/arrow_down.svg';

class FilterDropdown extends Component{

    wereItemsSelected = false;

    constructor(props){
        super(props);
        this.state = this.props.btnData;
    }

    componentDidUpdate(){
        this.wereItemsSelected = this.areAnyItemsSelected();
    }

    componentWillMount(){
        document.addEventListener('click', this.outsideClickHandler, false);
    }

    componentWillUnmount(){
        document.removeEventListener( 'click', this.outsideClickHandler, false);
    }

    toggleDropdown = () => {
        this.state.open = !this.state.open;
        this.setState(this.state);
    }

    getDropdownElements = () => {
        this.state.dropdown = Object.values(this.state.dropdown);
        var dropdownItems = this.state.dropdown.map((item, index) => {
            return <FilterDropdownItem itemData={item} key={index} id={index} selectHandler={this.handleSelectEvent}/>;
        });
        return <div className={"dropdown-container " + (this.state.open ? 'open' : '')}>{dropdownItems}</div>;
    }

    handleSelectEvent = (index, isSelected) => {

        this.state.dropdown[index].selected = isSelected;
        console.log(this.wereItemsSelected);
        if(this.wereItemsSelected != this.areAnyItemsSelected()){
            this.wereItemsSelected = this.areAnyItemsSelected();
            this.props.selectHandler(this.props.id)
        }
    }

    areAnyItemsSelected = () => {
        for(var index in this.state.dropdown)
            if(this.state.dropdown[index].selected)
                return true;
        return false;
    }

    outsideClickHandler = (e) => {
        if((!this.node.contains(e.target) && e.target.className != "checkIcon") && this.state.open)
            this.toggleDropdown();
    }

    render(){
        return(
            <div className="dropdown-btn-container" ref={node => this.node = node}>
                <button className={"btn "+ (this.state.open || this.areAnyItemsSelected() ? 'active ' : '')} onClick={this.toggleDropdown}>
                    <div className="text-container">
                        <span className="category-title">{this.state.title}</span>
                        <img className="icon" src={this.state.open ? IconArrowUp : IconArrowDown}/>
                    </div>
                </button>
                {this.getDropdownElements()}
            </div>
        );
    }
}

export default FilterDropdown;