import React, { Component } from 'react';

class FilterButton extends Component{

    constructor(props){
        super(props);
        this.state = this.props.btnData;
    }

    render(){
        return(
            <button className={"btn "+(this.state.active ? 'active' : '')} onClick={()=>{this.props.clickHandler(this.props.id)}}>
                <div className="text-container">
                    <span className="category-title">{this.state.title}</span>
                    {(this.state.hasOwnProperty('subtitle')) ? (
                        <span className="category-subtitle">{this.state.subtitle}</span>
                    ): null }
                </div>
            </button>
        );
    }
}

export default FilterButton;