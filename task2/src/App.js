import React, { Component } from 'react';
import FilterMenu from './components/FilterMenu';
import './styles/styles.scss';

class App extends Component {

  getFilterMenuButtonData = () => {
    return [
          {title: "All", allButton: true, active:true},
          {title: "Small", subtitle: "$422+", active:false},
          {title: "Medium", subtitle: "$433+", active:false},
          {title: "Large", subtitle: "$456+", active:false},
          {title: "SUV", subtitle: "$525+", active:false},
          {title: "Van", subtitle: "$649+", active:false},
          {title: "More", open:false, active:false, dropdown:[
                {title: "Pickup Truck", subtitle:"$594", selected:false},
                {title: "Luxury", subtitle: "$626", selected:false},
                {title: "Commercial", subtitle: "$1248", selected: false},
                {title: "Convertible", subtitle: "$1607", selected: false},
              ]
          },
      ];
  }

  render() {
    return (
      <div className="App">
          <FilterMenu btnData={this.getFilterMenuButtonData()}/>
      </div>
    );
  }
}

export default App;
