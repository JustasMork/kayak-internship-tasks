

package task1;

/**
 *
 * @author Justas
 */
public class FrequencyGraph 
{
    ListContainer list;
    int largestFrequency;
    public FrequencyGraph(ListContainer list)
    {
        this.list = list;
        largestFrequency = 0;
    }
    
    public void printGraph()
    {
        String[][] graphData = getElementsBarsArrays();
        String outputString = "";

        for(int i = largestFrequency; i >= 0; i--)
        {
            for(int j = 0; j < graphData.length; j++)
            {
                if(graphData[j].length > i)
                    outputString += " "+graphData[j][i]+" ";
                else
                    outputString += String.format(" %"+getElementCharCount(j)+"s ", " ");
            }
            outputString += "\n";
        }
        
        System.out.println(outputString);
    }
    
    String[][] getElementsBarsArrays() 
    {
        String[][] barsArrays = new String[list.size()][];
        for(int i = 0; i < list.size(); i++)
        {
            barsArrays[i] = getElementBarArray(i);
            if(list.get(i).getFrequency() > largestFrequency)
                largestFrequency = list.get(i).getFrequency();
        }
        
        return barsArrays;
    }
    
    String[] getElementBarArray(int index)
    {
        String[] barArray = new String[list.get(index).getFrequency() + 1];
        int elementCharCount = getElementCharCount(index);
        barArray[0] = String.format("%s", String.valueOf(list.get(index).getValue()));
        
        for(int i = 1; i <= list.get(index).getFrequency(); i++)
            barArray[i] = String.format("%"+elementCharCount+"s", "*");

        return barArray;   
    }
    
    int getElementCharCount(int index)
    {
        return String.valueOf(list.get(index).getValue()).length();
    }
}
