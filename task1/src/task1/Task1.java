package task1;

import task1.Utils.ListContainerFactory;

/**
 *
 * @author Justas
 */
public class Task1
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        UserPrompt promt = new UserPrompt();
        try {
            ListContainer list = (new ListContainerFactory()).getListContainer(promt.askForAList());

            list.fillMissingElements();
            list.sort();

            promt.printFrequencyInformation(list);
            promt.printFrequencyInformationInGraph(list);
        } catch(Exception e){
            System.out.println("Error occurred, please check your data");
        }

    }
}
