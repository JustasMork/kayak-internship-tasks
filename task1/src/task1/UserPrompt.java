

package task1;

import java.util.Scanner;
import task1.Contracts.ListItem;

/**
 *
 * @author Justas
 */
public class UserPrompt 
{
    private final Scanner scanner;
    
    public UserPrompt()
    {
        scanner = new Scanner(System.in);
    }

    public String askForAList()
    {
        System.out.println("Please enter comma-separated numbers:");
        return scanner.nextLine();
    }
    
    public void printFrequencyInformation(ListContainer list)
    {     
        System.out.println("\nFrequency information:");
        
        String outputFrequency = String.format("%10s: ", "Frequency");
        String outputValues = String.format("%10s: ", "Value");
        
        for(ListItem item : list)
        {
            int maxNumberOfChars = getMaxNumberOfCharsInStrings(item.getValue().toString(), String.valueOf(item.getFrequency()));
            outputFrequency += String.format("%"+maxNumberOfChars+"d, ", item.getFrequency());
            outputValues += String.format("%"+maxNumberOfChars+"s, ", item.getValue().toString());
        }
        System.out.println(outputFrequency);
        System.out.println(outputValues);
    }
    
    public void printFrequencyInformationInGraph(ListContainer list)
    {
        System.out.println("\nFrequency information in graph:");
        FrequencyGraph graph = new FrequencyGraph(list);
        graph.printGraph();
    }

    int getMaxNumberOfCharsInStrings(String s1, String s2)
    {
        if(s1.length() > s2.length())
            return s1.length();
        return s2.length();
    }
}
