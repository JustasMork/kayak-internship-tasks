

package task1;

import task1.Contracts.ListItem;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Justas
 */
public class ListContainer implements Iterable<ListItem>
{
    
    protected ListItem maxValue;
    
    protected ListItem minValue;
    
    protected ArrayList<ListItem> list;
    
    
    public ListContainer() throws UnsupportedOperationException
    {
        list = new ArrayList<>();
    }
    
    public ListItem get(int index)
    {
        return list.get(index);
    }
    
    public ListItem getMin()
    {
        return minValue;
    }
    
    public ListItem getMax()
    {
        return maxValue;
    }
    
    public int size()
    {
        return list.size();
    }
    
    public void addElementToList(ListItem listItem)
    {
        setMaxIfLarger(listItem);
        setMinIfSmaller(listItem);
        
        int itemIndex = list.indexOf(listItem);

        if(itemIndex == -1)
        {
            listItem.incrementFrequency();
            list.add(listItem);
        } else 
            list.get(itemIndex).incrementFrequency();
        
    }
    
    protected boolean setMaxIfLarger(ListItem listItem)
    {
        if(maxValue == null || maxValue.compareTo(listItem) == -1)
        {
            maxValue = listItem;
            return true;
        }
        return false;
    }
    
    protected boolean setMinIfSmaller(ListItem listItem)
    {
        if(minValue == null || minValue.compareTo(listItem) == 1)
        {
            minValue = listItem;
            return  true;
        }
        return false;
    }
    
    public void sort()
    {
        list.sort((ListItem a, ListItem b) -> a.compareTo(b) );
    }
   
    
    public void fillMissingElements()
    {
        ListItem current = minValue;
        while(current.compareTo(maxValue) == -1)
        {
            current = current.getFollowingElement();
            if(!list.contains(current))
                list.add(current);
        }
    }

    @Override
    public Iterator<ListItem> iterator()
    {
        return list.iterator();
    }

}
