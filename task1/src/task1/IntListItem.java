

package task1;

import task1.Contracts.ListItem;

/**
 *
 * @author Justas
 */
public class IntListItem implements ListItem<Integer>
{
    
    protected Integer value;
    
    protected Integer frequency;
    
    public IntListItem(int itemValue)
    {
        value = itemValue;
        frequency = 0;
    }

    @Override
    public Integer getValue()
    {
        return value;
    }

    @Override
    public int getFrequency()
    {
        return frequency;
    }

    @Override
    public void incrementFrequency()
    {
        frequency++;
    }

    @Override
    public int compareTo(ListItem o)
    {
        if(o instanceof IntListItem)
            return value.compareTo(((IntListItem)o).getValue());
        else
            throw new IllegalArgumentException("Illegal comparation of different type elements");
    }

    @Override
    public int hashCode()
    {
        return value.hashCode()+frequency.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof IntListItem)
            return getValue() == ((IntListItem) obj).getValue();
        return false;
    }

    @Override
    public IntListItem getFollowingElement()
    {
        return new IntListItem(getValue()+1);
    }

}
