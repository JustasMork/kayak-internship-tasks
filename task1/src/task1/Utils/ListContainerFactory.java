

package task1.Utils;

import task1.ListContainer;

/**
 *
 * @author Justas
 */
public class ListContainerFactory 
{
    protected ListItemFactory itemFactory;
    
    public ListContainerFactory()
    {
        itemFactory = new ListItemFactory();
    }
    
    public ListContainer getListContainer(String inputString)
    {
        ListContainer listContainer = new ListContainer();
        String[] elementsStrings  = inputString.split(",");
       
       for(String element : elementsStrings)
           listContainer.addElementToList(itemFactory.getItemFromUserInput(element));
       
       return listContainer;
    }
}
