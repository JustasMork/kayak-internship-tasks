

package task1.Utils;

import task1.Contracts.ListItem;
import task1.IntListItem;

/**
 *
 * @author Justas
 */
public class ListItemFactory 
{
    public ListItem getItemFromUserInput(String itemValue) throws UnsupportedOperationException
    {
        itemValue = itemValue.trim();

        int item = Integer.parseInt(itemValue);
        return getItem(item);
    }
    
    public ListItem getItem(int itemValue)
    {
        return new IntListItem(itemValue);
    }
    
    public ListItem getItem(char itemValue) throws UnsupportedOperationException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
