/*
 */
package task1.Contracts;

/**
 *
 * @author Justas
 * @param <T>
 */
public interface ListItem<T> extends Comparable<ListItem>
{
    
    public T getValue();
    
    public int getFrequency();
    
    public ListItem getFollowingElement();
    
    public void incrementFrequency();
    
    @Override
    public boolean equals(Object obj);
    
    @Override
    public int hashCode();
    
}
